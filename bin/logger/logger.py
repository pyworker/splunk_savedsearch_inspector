# v.1.3
# coding=utf-8

import json
import logging
import os
import re
from logging.config import dictConfig

DEFAULT_LEVEL = "INFO"

LOGGER_FOLDER = '../var/logs'
CONF_FILE = "logger_conf.json"


def set_path_enviroment(config):
    """
    подстановка в конфиг переменных среды
    :param config: конфигугационный файл логера
    :return:
    """
    pattern = re.compile('\$(.*?)\$')
    handlers = config.get('handlers')
    for handler in handlers:
        handler = handlers.get(handler)
        if handler.get('filename') and pattern.match(handler.get('filename')):
            sp = handler.get('filename').split('$')
            path_enviroment = sp[1]
            path = sp[2]
            lf = ''.join([os.environ.get(path_enviroment), path])
            handler.update({'filename': lf})
    return config


def logger_setup(logger='log'):
    """
    функция настройки логера
    :param logger:
    :return:
    """
    logger_dir = os.path.dirname(os.path.realpath(__file__))
    conf_path = os.path.join(logger_dir, CONF_FILE)

    # создается диретокрия для локального хранения логов
    # if not os.path.exists(os.path.join(logger_dir, LOGGER_FOLDER)):
    #     os.makedirs(os.path.join(os.getcwd(), LOGGER_FOLDER))
    logger = logging.getLogger(logger)

    if os.path.exists(conf_path):
        print('find log config - ok')
        try:
            with open(conf_path, 'r') as f:
                config = json.load(f)
        except IOError as io_err:
            raise io_err

        config = set_path_enviroment(config)
        dictConfig(config)
    else:
        print('find log config - error')
        logging.basicConfig(level=DEFAULT_LEVEL)

    return logger


if __name__ == '__main__':
    logger_setup()



