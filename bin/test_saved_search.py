import unittest
from saved_search import SavedSearch

TEST_SAVEDSEARCH_NAME = 'test_test_test'
TEST_SAVEDSEARCH_QUERY = '* | head 10'
TEST_APP = 'alert_manager'
TEST_OWNER = 'admin'

class TestSevedSearch(unittest.TestCase):
    def test_create_run_delete_savedsearch(self):
        s = SavedSearch(app=TEST_APP, owner=TEST_OWNER)
        # preparation for the test
        try:
            s.delete(name=TEST_SAVEDSEARCH_NAME)
        except Exception as ex:
            # ignore
            pass

        s.create(name=TEST_SAVEDSEARCH_NAME, query=TEST_SAVEDSEARCH_QUERY)
        job = s.run(name=TEST_SAVEDSEARCH_NAME)
        s.delete(name=TEST_SAVEDSEARCH_NAME)
        ans = s.get_by_name(name=TEST_SAVEDSEARCH_NAME)
        should_ans = None
        self.assertEqual(ans, should_ans)

    def test_create_delete_alert(self):
        s = SavedSearch(app=TEST_APP, owner=TEST_OWNER)
        # preparation for the test
        try:
            s.delete(name=TEST_SAVEDSEARCH_NAME)
        except Exception as ex:
            # ignore
            pass

        s.create_alert( name=TEST_SAVEDSEARCH_NAME, query=TEST_SAVEDSEARCH_QUERY)
        s.delete(name=TEST_SAVEDSEARCH_NAME)

        # preparation for the test
        try:
            s.delete(
            name=TEST_SAVEDSEARCH_NAME,
            owner=TEST_OWNER,
            app=TEST_APP,
            sharing='user'
        )
        except Exception as ex:
            # ignore
            pass

        s.create_alert(
            name=TEST_SAVEDSEARCH_NAME,
            query=TEST_SAVEDSEARCH_QUERY,
            owner=TEST_OWNER,
            app=TEST_APP,
            sharing='user'
        )

        s.delete(
            name=TEST_SAVEDSEARCH_NAME,
            owner=TEST_OWNER,
            app=TEST_APP,
            sharing='user'
        )

        ans = s.get_by_name(name=TEST_SAVEDSEARCH_NAME)
        should_ans = None
        self.assertEqual(ans, should_ans)


if __name__ == '__main__':
    unittest.main()
