from saved_search import SavedSearch
from logger import logger_setup

if __name__ == '__main__':
    log = logger_setup()
    s = SavedSearch(app='alert_manager', owner='admin')
    saved_searches = s.get_all()
    s.print_search(saved_searches)
    for saved_searche in saved_searches:
        message = '"savedsearch": "%s",' % saved_searche.name
        jobs = s.get_jobs(savedsearch=saved_searche)

        for job in jobs:
            # log.info(job.get('sid'))
            res = s.get_job_result(job=job.get('job'))
            message = ' '.join([message, res])
        
        print(message)
        print('===========================================')
