"""
module for working with saved search
"""
import sys
import splunk
import splunk.search

import otcommon.splunklib.client as client
import otcommon.splunklib.binding
import otcommon.splunklib.results as results

from utils import get_splunk_session_key

class SavedSearch:
    """working with Splunk SavedSearch"""
    def __init__(self, app=None, owner=None):
        """
        Args:
            app: str, Splunk application name (ex: 'alert_manager')
            owner: str, Splunk username (ex: 'nobody')
        Raises:
            Exception: cant get session key
        """
        try:
            self.session_key = get_splunk_session_key()
        except Exception as ex:
            raise Exception('failed to connect to Splunk: %s' % ex)

        if app and owner:
            self.service = client.connect(token=self.session_key, app=app, owner=owner, sharing='app')
        else:
            self.service = client.connect(token=self.session_key, sharing='app')

        # 1: create private alert
        # namespace = client.namespace(owner='admin', app='search', sharing='user')

        # 2: create sharing alert
        # self.namespace = client.namespace(owner='admin', app='alert_manager', sharing='app')

    def create(self, name, query, owner=None, app=None, sharing=None):
        """
        Create a saved search
        Do not include the 'search' keyword for a saved search
        Args:
            name: str, name new savedsearch
            query: str, search query in a SPL language
            owner: str, Splunk username (ex: 'admin')
            app: str, Splunk application name (ex: 'search')
            sharing: str, rights of access to saved search(ex: 'user')
        Raises:
            Exception: A saved search with that name already exists.
                     : raise HTTP Error
        Returns:    
            True if saved search created
        """
        
        namespace = None
        if owner and app and sharing:
            namespace = client.namespace(owner=owner, app=app, sharing=sharing)

        try:
            if namespace:
                new_savedsearch = self.service.saved_searches.create(name=name, search=query, namespace=self.namespace)
            else:
                new_savedsearch = self.service.saved_searches.create(name=name, search=query)

        except otcommon.splunklib.binding.HTTPError as HTTPError:
            raise Exception('failed to create new saved search: %s' % HTTPError.message)
        except Exception as ex:
            raise Exception('failed to create new saved search: %s' % ex)
        return True


    def create_alert(self, name, query, owner=None, app=None, sharing=None):
        """
        create new alert
        Args:
            name: str, name new savedsearch
            query: str, search query in a SPL language
            owner: str, Splunk username (ex: 'admin')
            app: str, Splunk application name (ex: 'search')
            sharing: str, rights of access to saved search(ex: 'user')
        Raises:
            Exception: A saved search with that name already exists.
                     : raise HTTP Error
        Returns:    
            True if saved search created
        """

        namespace = None
        if owner and app and sharing:
            namespace = client.namespace(owner=owner, app=app, sharing=sharing)

        kwargs = {
            'alert_type': 'number of events',
            'alert.severity': '2',
            'alert.suppress': False,
            'alert.track': True,
            'dispatch.earliest_time': '-1h',
            'dispatch.latest_time': 'now',
            'dispatch.max_time': 60,
            'cron_schedule': '* * * * *',
            'is_scheduled': True,
            'alert_comparator': 'greater than',
            'alert_threshold': 0,
            'is_visible': True,
            'max_concurrent': 1,
            'action.script': True,
            'action.script.command': 'sendalert $action_name$ results_file="$results.file$" results_link="$results.url$"',
            'action.script.maxresults': 10000,
            # dont work "action.script.filename"
            'action.script.filename': 'test_filename.py',
            'action.script.ttl': 600,
            'action.script.maxtime': '5m'
        }

        try:
            if namespace:

                #
                # create alert with special namespace
                #
                new_savedsearch = self.service.saved_searches.create(
                    name=name,
                    search=query,
                    namespace=namespace,
                    **kwargs
                )
            else:

                #
                # create alert with common namespace
                #
                new_savedsearch = self.service.saved_searches.create(
                    name=name,
                    search=query,
                    **kwargs
                )

        except otcommon.splunklib.binding.HTTPError as HTTPError:
            raise Exception('failed to create new saved search: %s' % HTTPError.message)
        except Exception as ex:
            raise Exception('failed to create new saved search: %s' % ex)
        return True

    def run_exec(self, name):
        """
        execute savedsaerch
        Args:
        Raises:
            Exception: cant find saved search
        Returns: 
            new Job object
        """
        try:
            job = splunk.search.dispatchSavedSearch(
                        name,
                        self.session_key,
                        earliestTime='-24h@h',
                        latestTime='now',
                        auto_cancel='0'
                    )
        except splunk.ResourceNotFound as not_found:
            raise Exception('failed to run savedsearch: %s' % not_found)
        return job

    def run(self, name):
        """
        run savedsearch
        Args:
            name: str, savedsearch name 
        Raises:
            Exception: saved search not found
        Returns:
            new Job object
        """
        try:
            savedsearch = self.get_by_name(name=name)
            job = savedsearch.dispatch()
        except splunk.ResourceNotFound as not_found:
            raise Exception('failed to run savedsearch: %s' % not_found)
        except Exception as ex:
            raise Exception('failed to run savedsearch: %s' % ex)
        return job

    def get_job_result(self, job):
        # Wait for the job to finish--poll for completion and display stats
        while True:
            job.refresh()
            stats = {
                'sid': job.sid,
                'isDone': job.isDone,
                'doneProgress': float(job.doneProgress)*100,
                'scanCount': int(job.scanCount),
                'eventCount': int(job.eventCount),
                'resultCount': int(job.resultCount),
                'dispatchState': job.dispatchState,
                # 'max_count': job.max_count,
                'runDuration': float(job.runDuration),
                'searchProviders': job.searchProviders
            }
            status = (
                '\r"sid": "%(sid)s", "dispatchState"="%(dispatchState)s", doneProgress="%(doneProgress)03.1f", '
                'scanCount: "%(scanCount)d", "eventCount": "%(eventCount)d", "resultCount": "%(resultCount)d", '
                '"runDuration": "%(runDuration).2f", "searchProviders": "%(searchProviders)s"') % stats

            # sys.stdout.write(status)
            # sys.stdout.flush()
            break
            # if stats['isDone'] == '1':
            #     break
            # sleep(2)

        # Display the search results now that the job is done
        # jobresults = job.results()

        # while True:
        #     content = jobresults.read(1024)
        #     if len(content) == 0: break
        #     # sys.stdout.write(content)
        #     sys.stdout.flush()
        # sys.stdout.write("\n")
        return status

    def get_all(self):
        """
        get list of saved searches
        that are available to the current user
        Args:
            name: str, saved search name
        Raises:
            Exception: unexpected error
        Returns:
            list of saved searches
        """
        try:
            return self.service.saved_searches
        except Exception as ex:
            raise Exception('failed to get the list of saved searches: %s' % ex)

    def print_savedsearches(self, savedsearches):
        """
        print list of saved searches
        Args:
            savedsearches: list, saved searches
        Raises:

        Returns:
        """
        for savedsearch in savedsearches:
            print("  %s" % savedsearch.name)
            print("      Query: %s" % savedsearch.search)
            print("  %s" % savedsearch.path)

    def get_by_name(self, name, owner=None, app=None, sharing=None):
        """
        get the saved search by name
        Args:
            name: str, saved search name 
            owner: str, Splunk username
            app: str, Splunk application name
            sharing: str, rights of access to savedsearch
        Returns:
            Savedsearch object
            None: cant find savedsearch
        """
        namespace = None
        if owner and app and sharing:
            # create sharing alert
            namespace = client.namespace(owner='admin', app='search', sharing='app')

        try:
            if namespace:
                savedsearch = self.service.saved_searches[name, namespace]
            else:
                savedsearch = self.service.saved_searches[name]

        except KeyError as key_err:
            # failed to get saved search
            return None
        return savedsearch

    def get_jobs(self, savedsearch):
        """
        get the jobs history of saved searches
        Args:
            name: str, saved search name 
        Returns:
            list of jobs for saved search
        """
        jobs = []
        history = savedsearch.history()
        if len(history) > 0:
            for job in history:

                #################################
                # stop all savedsearch jobs
                # job.cancel()

                # run job again
                # job = self.saved_search.dispatch()
                # while not job.is_ready():
                #     sleep(0.1)
                # history = self.saved_search.history()
                #################################

                stats = {
                    'job': job,
                    'name': job.name,
                    'runDuration': job.runDuration,
                    # 'updated': job.updated,
                    'path': job.path,
                    'isDone': job.isDone,
                    'isPaused': job.isPaused,
                    'sid': job.sid,
                    'doneProgress': float(job.doneProgress)*100,
                    'scanCount': int(job.scanCount),
                    'eventCount': int(job.eventCount),
                    'resultCount': int(job.resultCount)
                }

                jobs.append(stats)
        else: 
            print "No jobs for savedsearch %s" % savedsearch.name
            return []

        return jobs

    def delete(self, name, owner=None, app=None, sharing=None):
        """
        delete the saved search
        Args:
            name: str, saved search name 
            owner: str, Splunk username
            app: str, Splunk application name
            sharing: str, rights of access to savedsearch
        Raises:
            Exception: invalid saved search name
                     : global error
        Returns:
            True if saved search deleted
        """

        namespace = None
        if owner and app and sharing:
            namespace = client.namespace(owner=owner, app=app, sharing=sharing)

        try:
            if namespace:
                self.service.saved_searches.delete(name=name, namespace=namespace)
            else:
                self.service.saved_searches.delete(name=name)

        except KeyError as key_err:
            raise Exception('failed to delete saved search: invalid name %s:%s' % (name, key_err.message))
        except Exception as ex:
            raise Exception('failed to delete saved search: %s' % ex)
        return True


class ISavedSearch:
    """docstring for ISavedSearch"""
    def __init__(self, savedsearch):
        name = savedsearch['name']
        self.query = savedsearch['query']
